
/*
Cephes Math Library Release 2.1:  December, 1988
Copyright 1984, 1987, 1988 by Stephen L. Moshier
Direct inquiries to 30 Frost Street, Cambridge, MA 02140
*/


double polevl ( double x, double coef[100], int N ) {
  double ans;
  int i;
  //double *p;

  int j = 0;
  //p = coef;
  //ans = *p++;
  ans = coef[j++];
  i = N;
  
  do
    ans = ans * x  +  coef[j++];
  while ( --i );

  //---do
  //---  ans = ans * x  +  *p++;
  //---while ( --i );
  
  return ans;
}



/*		p1evl()	*/
/*              N
 * Evaluate polynomial when coefficient of x  is 1.0.
 * Otherwise same as polevl.
 */

double p1evl ( double x, double coef[100], int N ) {
  double ans;
  //double *p;
  int i;
  
  int j = 0;
  //p = coef;
  //ans = x + *p++;
  ans = x + coef[j++];
  i = N-1;
  
  do
    ans = ans * x  + coef[j++] ;
  while ( --i );

  //---do
  //---  ans = ans * x  + *p++;
  //---while ( --i );
  
  return ans;
}
