/*	
 * Square root
 * y = sqrt( x );
 *
 * DESCRIPTION:
 *
 * Returns the square root of x.
 *
 * Range reduction involves isolating the power of two of the
 * argument and using a polynomial approximation to obtain
 * a rough value for the square root.  Then Heron's iteration
 * is used three times to converge to an accurate value.
 *
 * ACCURACY:
 *                      Relative error:
 * arithmetic   domain     # trials      peak         rms
 *    IEEE      0,1.7e308   30000       1.7e-16     6.3e-17
 *
 * ERROR MESSAGES:
 *   message         condition      value returned
 * sqrt domain        x < 0            0.0
 */

/*
Cephes Math Library Release 2.8:  June, 2000
Copyright 1984, 1987, 1988, 2000 by Stephen L. Moshier
*/

#include "sqrt.h"

double f_sqrt ( double x ) {
  int e;
  double z, w;
  
  if ( x <= 0.0 )  return  0.0 ;
  
  w = x;
  // separate exponent and significand 
  z = f_frexp ( x, &e );
  
  // approximate square root of number between 0.5 and 1
  // relative error of approximation = 7.47e-3
  x = 4.173075996388649989089E-1 + 5.9016206709064458299663E-1 * z;
  
  // adjust for odd powers of 2
  if( (e & 1) != 0 ) x *= SQRT2;
  
  //  re-insert exponent 
  x = f_ldexp( x, (e >> 1) );
  
  // Newton iterations:
  x = 0.5* (x + w/x);
  x = 0.5* (x + w/x);
  x = 0.5* (x + w/x);
  
  return(x);
}
