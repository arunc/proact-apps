
/*
Cephes Math Library Release 2.8:  June, 2000
Copyright 1985, 1995, 2000 by Stephen L. Moshier
*/

#include "sin.h"

double f_sin ( double x ) {
  double y, z, zz;
  int j, sign;
  
  if( x == 0.0 )  return x;
  sign = 1; 
  if( x < 0 ) { // make argument positive but save the sign
    x = -x;
    sign = -1;
  }
  
  if( x > lossth ) {
    return 0.0 ;
  }
  
  y = f_floor ( x/PIO4 ); /* integer part of x/PIO4 */
  
/* strip high bits of integer part to prevent integer overflow */
  z = f_ldexp( y, -4 );
  z = f_floor(z);           /* integer part of y/8 */
  z = y - f_ldexp( z, 4 );  /* y - 16 * (y/16) */
  
  j = z; /* convert to integer for tests on the phase angle */
/* map zeros to origin */
  if( j & 1 ) {
    j += 1;
    y += 1.0;
  }
  j = j & 07; /* octant modulo 360 degrees */
/* reflect in x axis */
  if( j > 3) {
    sign = -sign;
    j -= 4;
  }
  
/* Extended precision modular arithmetic */
  z = ((x - y * DP1) - y * DP2) - y * DP3;
  
  zz = z * z;
  
  if( (j==1) || (j==2) ) {
    y = 1.0 - f_ldexp (zz,-1) + zz * zz * polevl ( zz, coscof, 5 );
  }
  else {
    // y = z  +  z * (zz * polevl( zz, sincof, 5 ));
    y = z  +  z * z * z * polevl ( zz, sincof, 5 );
  }
  
  if (sign < 0) y = -y;
  
  return y;
}


double f_cos ( double x ) {
  double y, z, zz;
  long i;
  int j, sign;
  
   sign = 1;
  if ( x < 0 ) // make argument positive 
    x = -x;
  
  if( x > lossth ) {
    return 0.0;
  }
  
  y = f_floor ( x/PIO4 );
  z = f_ldexp ( y, -4 );
  z = f_floor (z);		/* integer part of y/8 */
  z = y - f_ldexp ( z, 4 );  /* y - 16 * (y/16) */
  
/* integer and fractional part modulo one octant */
  i = z;
  if( i & 1 )	/* map zeros to origin */
  {
    i += 1;
    y += 1.0;
  }
  j = i & 07;
  if( j > 3)
  {
    j -=4;
    sign = -sign;
  }
  
  if( j > 1 )
    sign = -sign;
  
/* Extended precision modular arithmetic */
  z = ((x - y * DP1) - y * DP2) - y * DP3;
  
  zz = z * z;
  
  if( (j==1) || (j==2) ) {
    // y = z  +  z * (zz * polevl( zz, sincof, 5 ));
    y = z  +  z * z * z * polevl( zz, sincof, 5 );
  }
  else
  {
    y = 1.0 - f_ldexp (zz,-1) + zz * zz * polevl ( zz, coscof, 5 );
  }
  
  if (sign < 0)
    y = -y;
  
  return y;
}





/* Degrees, minutes, seconds to radians: */

/* 1 arc second, in radians = 4.8481368110953599358991410e-5 */
static double P64800 = 4.8481368110953599358991410e-5;

double radian ( double d, double m, double s ) {

  return ( ((d*60.0 + m)*60.0 + s)*P64800 );
}

