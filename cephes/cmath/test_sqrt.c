

#include <stdio.h>
#include <math.h>
#include "sqrt.h"


void f_print (double x) {
  printf ( " sqrt of  %f = %f   [ math.h : %f ] \n",   
	   x, f_sqrt(x), sqrt(x) );
}


int main () {
  f_print (0.5);

  f_print (1.0);
  f_print (1.5);
  f_print (2.0);
  f_print (2.5);
  f_print (4.5);

  f_print (100.54);

  f_print (123456789.123456789);


  f_print (0.0005);

  f_print (0.000001);

  return 0;
}

