
#include "math_utils.h"

double f_floor ( double x ) {
  return  (double)( (long)x -  ( (long)x % 1 ) );
  
}

double f_ceil ( double x ) {
  return  ( -1.0 *  f_floor ( -x ) );
}


// returns x*2^exp
double f_ldexp (double x, int exp) {
  if ( 0 == exp ) return 1.0;
  int flag = 1;
  if ( exp < 0 ) { 
    flag = -1; 
    exp = exp * -1;
  }

  double res = 1.0;
  int i = 0;
  for (i=0; i<exp; i++) {
    res = res * 2.0;
  }
  if (-1 == flag)  res = 1.0 / res ;
  
  return x * res ;
}

// for the timebeing!
int f_isnan ( double x ) {
  return 0;
}

// for the timebeing!
int f_isfinite ( double x ) {
  return 1;
}




// http://stackoverflow.com/questions/22996193/union-binary-to-double
double f_frexp(double number, int *exp) {
  static const unsigned long long mantissa_mask       = 0x000FFFFFFFFFFFFFllu;
  static const unsigned long long mantissa_impliedBit = 0x0010000000000000llu;
  static const unsigned long long expo_mask           = 0x7FF0000000000000llu;
  static const unsigned long long expo_norm           = 0x3FE0000000000000llu;
  static const unsigned long long sign_mask           = 0x8000000000000000llu;
  static const int expo_NaN = 0x07FF;
  static const int expo_Bias = 1023;

  union {
    double d;
    unsigned long long u;
  } x = { number };

  unsigned long long mantissa = x.u & mantissa_mask;
  int expo = (x.u & expo_mask) >> 52;

  if (expo == expo_NaN) {  // Behavior for Infinity and NaN is unspecified.
    *exp = 0;
    return number;
  }
  if (expo > 0) {
    mantissa |= mantissa_impliedBit;  // This line is illustrative, not needed.
    expo -= expo_Bias;
  }
  else if (mantissa == 0) {
    *exp = 0;
    return number;  // Do not return 0.0 as that does not preserve -0.0
  }
  else {
    // de-normal or sub-normal numbers
    expo = 1 - expo_Bias;  // Bias different when biased exponent is 0
    while (mantissa < mantissa_impliedBit) {
      mantissa <<= 1;
      expo--;
    }
  }
  *exp = expo + 1;
  mantissa &= ~mantissa_impliedBit;
  
  x.u = (x.u & sign_mask) | expo_norm | mantissa;


  //---// assume we get proper exponent here.
  //---// number = fract * 2^exp 
  //---// therefore, fract = number / (2 ^ exp)
  //---int flag = 1;
  //---int expval = *exp ;
  //---
  //---if ( expval < 0 ) { 
  //---  flag = -1; 
  //---  expval = expval * -1;
  //---}
  //---double res = 1.0;
  //---int i = 0;
  //---for (i=0; i<expval; i++) {
  //---  res = res * 2.0;
  //---}
  //---if (-1 == flag)  res = 1.0 / res ;
  //---
  //---printf ("expval =  %d   number = %f  res = %f \n", expval, number, res);
  //---//return number / (res);

  return x.d;
}
