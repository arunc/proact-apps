#pragma once
#ifndef MATH_UTILS_H
#define MATH_UTILS_H

// The functions here serve as a replacement for math.h
// all the math functions are prefixed by a f_

double f_floor ( double x );
double f_ceil ( double x );
double f_ldexp (double x, int exp);

int f_isnan ( double x );
int f_isfinite ( double x );

double f_frexp(double number, int *exp);

#endif
