

#include <stdio.h>
#include <math.h>
#include "sin.h"


void f_print (double x) {
  printf ( " sin of  %f = %f   [ math.h : %f ] \n",   
	   x, f_sin(x), sin(x) );
}


void ff_print (double x) {
  printf ( " cos of  %f = %f   [ math.h : %f ] \n",   
	   x, f_cos(x), cos(x) );
}


int main () {
  f_print (0.5);
  f_print (1.0);
  f_print (1.5);
  f_print (2.0);
  f_print (2.5);
  f_print (4.5);


  ff_print (0.5);
  ff_print (1.0);
  ff_print (1.5);
  ff_print (2.0);
  ff_print (2.5);
  ff_print (4.5);


}

