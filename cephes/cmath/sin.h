#pragma once
#ifndef SIN_H
#define SIN_H


#include "mconf.h"
#include "polevl.h"
#include "math_utils.h"

 /*	Circular sine
 * SYNOPSIS:
 * y = sin( x );
 * DESCRIPTION:
 *
 * Range reduction is into intervals of pi/4.  The reduction
 * error is nearly eliminated by contriving an extended precision
 * modular arithmetic.
 *
 * Two polynomial approximating functions are employed.
 * Between 0 and pi/4 the sine is approximated by
 *      x  +  x**3 P(x**2).
 * Between pi/4 and pi/2 the cosine is represented as
 *      1  -  x**2 Q(x**2).
 *
 * ACCURACY:
 *                      Relative error:
 * arithmetic   domain      # trials      peak         rms
 *    IEEE -1.07e9,+1.07e9  130000       2.1e-16     5.4e-17
 * 
 * ERROR MESSAGES:
 *
 *   message           condition        value returned
 * sin total loss   x > 1.073741824e9      0.0
 *
 * Partial loss of accuracy begins to occur at x = 2**30
 * = 1.074e9.  The loss is not gradual, but jumps suddenly to
 * about 1 part in 10e7.  Results may be meaningless for
 * x > 2**49 = 5.6e14.  The routine as implemented flags a
 * TLOSS error for x > 2**30 and returns 0.0.
 */
 /*	cos.c
 *
 *	Circular cosine
 * y = cos( x );
 *
 * DESCRIPTION:
 *
 * Range reduction is into intervals of pi/4.  The reduction
 * error is nearly eliminated by contriving an extended precision
 * modular arithmetic.
 *
 * Two polynomial approximating functions are employed.
 * Between 0 and pi/4 the cosine is approximated by
 *      1  -  x**2 Q(x**2).
 * Between pi/4 and pi/2 the sine is represented as
 *      x  +  x**3 P(x**2).
 *
 *
 * ACCURACY:
 *
 *                      Relative error:
 * arithmetic   domain      # trials      peak         rms
 *    IEEE -1.07e9,+1.07e9  130000       2.1e-16     5.4e-17
 */

/*
Cephes Math Library Release 2.8:  June, 2000
Copyright 1985, 1995, 2000 by Stephen L. Moshier
*/


static double sincof[] = {
  1.58962301576546568060E-10,
  -2.50507477628578072866E-8,
  2.75573136213857245213E-6,
  -1.98412698295895385996E-4,
  8.33333333332211858878E-3,
  -1.66666666666666307295E-1,
};

static double coscof[6] = {
  -1.13585365213876817300E-11,
  2.08757008419747316778E-9,
  -2.75573141792967388112E-7,
  2.48015872888517045348E-5,
  -1.38888888888730564116E-3,
  4.16666666666665929218E-2,
};

static double DP1 =   7.85398125648498535156E-1;
static double DP2 =   3.77489470793079817668E-8;
static double DP3 =   2.69515142907905952645E-15;
/* static double lossth = 1.073741824e9; */

static double lossth = 1.073741824e9;

double f_sin (double x);
double f_cos (double x);
double radian (double d, double m, double s);


#endif

