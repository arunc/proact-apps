#!/usr/bin/env bash
#
# Dont do anything, just run this script from the top directory.
#


command -v sshpass >/dev/null 2>&1 || { echo >&2 "Program sshpass is not installed. Either install sshpass or copy all these files manually.  Aborting."; exit 1; }
command -v scp >/dev/null 2>&1 || { echo >&2 "Program scp is not installed. Must have scp and ssh to communicate with zedboard.  Aborting."; exit 1; }



sshpass -p 'root' scp ./utils/fesvr-zedboard root@192.168.1.5:~/
sshpass -p 'root' scp ./utils/pk root@192.168.1.5:~/
sshpass -p 'root' scp ./utils/hello root@192.168.1.5:~/
sshpass -p 'root' scp ./utils/libfesvr.so root@192.168.1.5:/usr/local/lib/

sshpass -p 'root' ssh root@192.168.1.5 -t 'rm -f ~/fesvr-zynq'
