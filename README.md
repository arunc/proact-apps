 Application development targeted for PROACT - Processor for On Demand Approximate Computing
------------------------------------------------------------------------------
## Arun Chandrasekharan < arun@uni-bremen.de >

## Group of Computer Architecture, (AGRA)
## Dept. of Computer Science - University of Bremen, Germany.
## http://www.informatik.uni-bremen.de/agra/eng/index.php
------------------------------------------------------------------------------
## License : GPL-V3
------------------------------------------------------------------------------

For most of the stuff here to work, FIRST you must clone git@gitlab.com:arunc/proact-processor.git

SECOND, build the RISC-V tools as in proact-processor/build/HowToBuild.txt, and 

THIRD, set the RISC-V environment variable as in proact-processor/set_env.sh

These steps will set the gcc cross-compiler tool chain for ProACt


################################################################################

This README has to be completed.

For now, follow the steps in ./image_processing/readMe.txt  and ./hyperbolic/readMe.txt

################################################################################

################################################################################
