
sinhx_c_src = \
	sinhx_main.c \
	syscalls.c \

sinhx_riscv_src = \
	crt.S \

sinhx_c_objs     = $(patsubst %.c, %.o, $(sinhx_c_src))
sinhx_riscv_objs = $(patsubst %.S, %.o, $(sinhx_riscv_src))

sinhx_riscv_bin = sinhx.riscv
$(sinhx_riscv_bin) : $(sinhx_c_objs) $(sinhx_riscv_objs)
	$(RISCV_LINK) $(sinhx_c_objs) $(sinhx_riscv_objs) -o $(sinhx_riscv_bin) $(RISCV_LINK_OPTS)

junk += $(sinhx_c_objs) $(sinhx_riscv_objs) \
        $(sinhx_host_bin) $(sinhx_riscv_bin)
