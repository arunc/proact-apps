#include "util.h"
#include "approximation_level.h"
#include "approximation.h"
#include "input.h"


int printf(const char* fmt, ...); // from common/syscalls.c
void setStats(int);               // from common/util.h


// Reuse maximum computations than redoing.
#define  SIZE  100

double global_fact = 1;
double global_num  = 1;
const int terms = SIZE;


double factorial_array[SIZE + 1] = {0};
double factorial (double num) {
  if (num == 0) {
    global_num = 1;
    global_fact = 1;
    return global_fact;
  }
  if (global_num == (num - 1)) {
    global_fact = global_fact * num;
    global_num = num;
    return global_fact;
  }
  if (global_num == (num - 2)) {
    global_fact = global_fact * (num-1) * num;
    global_num = num;
    return global_fact;
  }

  
  global_num = num;
  double i;
  double fact = 1;
  for (i=1; i<=num; i++) {
    fact = fact*i;
  }
  global_fact = fact;
  return fact;
}


double power_normal (double xx, int yy) {
  if (yy == 0) return 1.0;
  if (yy == 1) return xx;
  if (xx == 1) return 1.0;
  if (xx == 0) return 0.0;

  int i;
  double res = 1.0;
  for (i=1; i<= yy; i++) {
    res = res * xx;
  }

  return res;
}



double global_xx = 0.0;
double global_yy = 100.0;
double power_parray[SIZE + 1] = {0};
double power_marray[SIZE + 1] = {0};
double power (double xx, double yy) {
  if (yy == 0) return 1;
  if (global_xx == 0.0) { global_xx = xx; }
  if (global_yy == (yy - 1)) {
   global_xx = global_xx * xx;
   global_yy = yy;
   return global_xx;
  }


  if (global_yy == (yy - 2)) {
   global_xx = global_xx * xx;
   global_xx = global_xx * xx;
   global_yy = yy;
   return global_xx;
  }

  
  int i=1;
  global_xx = 1;
  for (i=1; i<=yy; i++) {
    global_xx = global_xx * xx;
  }
  global_yy = yy;
  return global_xx;

}




double etox (double x) {
// 1+x+x^2/2!+x^3/3!
  int i;
  double sum = 0;
  double p, f;

  for (i=0; i<terms; i++) {
    double p = power_normal (x, i);
    double f = factorial_array[i];
   
    double sign = 1;
    if (p < 0) { 
      p = p * -1;
      sign = -1;
    }
    double res = p / f;
    sum = sum + (sign * res);
  }

  return sum;
}


double y_array [SIZE + 1] = {0};
double xfactor = 0.0;
double x_plus_x = 0.0;
// cases less than 100 only considered here. Remaining ifs ignored for time being.
double log1plusy () {
  double y = x_plus_x;
  // ln (1+y) = y - (y2/2) + (y3/3) - (y4/4) + ...
  // 1+y = x; y => x-1
  int i = 0;
  double sign = -1;
  double sum = y;
  for (i = 2; i<terms-1; i++) { // 98 terms, debug crash.
    double yp = y_array [i];
    double temp =  ( (sign) * (yp / (double) i) );
    sum = sum + temp; 
    if (sign == -1) sign  = 1;
    else sign = -1;
  }

  return sum + xfactor;
}


double atanhx (double x) {
  // atanhx (x) = 0.5 * ln ( (1 + x) / (1 - x) ) )
  
  double lnp = log1plusy ();
  double result = 0.5 * lnp;
  return result;
}




int main () {

  int i;
  double number = _NUMBER_;
  
  // pre-compute the factorials and powers
  // works only if x_plus_x is < 100. Remaining multipliers simply not provided.
  x_plus_x = (1 + number) / (1 - number);
  
  if ( x_plus_x > 1 && x_plus_x < 10) { 
    xfactor = 2.302585093;  
    x_plus_x = x_plus_x / 10.0; 
  }
  if ( x_plus_x >= 10 && x_plus_x < 100) { 
    xfactor = 4.605170186;  
    x_plus_x = x_plus_x / 100.0; 
  }
  x_plus_x = x_plus_x - 1;

  double f, yp;
  for (i=0; i<terms; i++) {
    yp = power_normal (x_plus_x, i);
    y_array[i] = yp;
    f = factorial (i);
    factorial_array[i] = f;
  }

  set_approximation_level (APPROX_LEVEL, EXP_LEVEL);
  setStats(1);
  double result = atanhx (number);
  setStats(0);
  
  long mult = 1000000000; // precision of x 10^10, i.e 10 digits after decimal point.
  printf ("atanhx of %ld x 10^(-9)  =  %ld x 10^(-9)  [ approx = %d ]\n", 
	  (long)(number * mult), (long)(result * mult), APPROX_LEVEL);

  return 0;
}
