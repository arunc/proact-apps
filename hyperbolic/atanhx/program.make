#=======================================================================
# UCB CS250 Makefile fragment for benchmarks
#-----------------------------------------------------------------------
#
# Each benchmark directory should have its own fragment which
# essentially lists what the source files are and how to link them
# into an riscv and/or host executable. All variables should include
# the benchmark name as a prefix so that they are unique.
#

atanhx_c_src = \
	atanhx_main.c \
	syscalls.c \

atanhx_riscv_src = \
	crt.S \

atanhx_c_objs     = $(patsubst %.c, %.o, $(atanhx_c_src))
atanhx_riscv_objs = $(patsubst %.S, %.o, $(atanhx_riscv_src))

atanhx_host_bin = atanhx.host
$(atanhx_host_bin) : $(atanhx_c_src)
	$(HOST_COMP) $^ -o $(atanhx_host_bin)

atanhx_riscv_bin = atanhx.riscv
$(atanhx_riscv_bin) : $(atanhx_c_objs) $(atanhx_riscv_objs)
	$(RISCV_LINK) $(atanhx_c_objs) $(atanhx_riscv_objs) -o $(atanhx_riscv_bin) $(RISCV_LINK_OPTS)

junk += $(atanhx_c_objs) $(atanhx_riscv_objs) \
        $(atanhx_host_bin) $(atanhx_riscv_bin)
