#=======================================================================
# UCB CS250 Makefile fragment for benchmarks
#-----------------------------------------------------------------------
#
# Each benchmark directory should have its own fragment which
# essentially lists what the source files are and how to link them
# into an riscv and/or host executable. All variables should include
# the benchmark name as a prefix so that they are unique.
#

asinhx_c_src = \
	asinhx_main.c \
	syscalls.c \

asinhx_riscv_src = \
	crt.S \

asinhx_c_objs     = $(patsubst %.c, %.o, $(asinhx_c_src))
asinhx_riscv_objs = $(patsubst %.S, %.o, $(asinhx_riscv_src))

asinhx_host_bin = asinhx.host
$(asinhx_host_bin) : $(asinhx_c_src)
	$(HOST_COMP) $^ -o $(asinhx_host_bin)

asinhx_riscv_bin = asinhx.riscv
$(asinhx_riscv_bin) : $(asinhx_c_objs) $(asinhx_riscv_objs)
	$(RISCV_LINK) $(asinhx_c_objs) $(asinhx_riscv_objs) -o $(asinhx_riscv_bin) $(RISCV_LINK_OPTS)

junk += $(asinhx_c_objs) $(asinhx_riscv_objs) \
        $(asinhx_host_bin) $(asinhx_riscv_bin)
