#=======================================================================
# UCB CS250 Makefile fragment for benchmarks
#-----------------------------------------------------------------------
#
# Each benchmark directory should have its own fragment which
# essentially lists what the source files are and how to link them
# into an riscv and/or host executable. All variables should include
# the benchmark name as a prefix so that they are unique.
#

coshx_c_src = \
	coshx_main.c \
	syscalls.c \

coshx_riscv_src = \
	crt.S \

coshx_c_objs     = $(patsubst %.c, %.o, $(coshx_c_src))
coshx_riscv_objs = $(patsubst %.S, %.o, $(coshx_riscv_src))

coshx_host_bin = coshx.host
$(coshx_host_bin) : $(coshx_c_src)
	$(HOST_COMP) $^ -o $(coshx_host_bin)

coshx_riscv_bin = coshx.riscv
$(coshx_riscv_bin) : $(coshx_c_objs) $(coshx_riscv_objs)
	$(RISCV_LINK) $(coshx_c_objs) $(coshx_riscv_objs) -o $(coshx_riscv_bin) $(RISCV_LINK_OPTS)

junk += $(coshx_c_objs) $(coshx_riscv_objs) \
        $(coshx_host_bin) $(coshx_riscv_bin)
