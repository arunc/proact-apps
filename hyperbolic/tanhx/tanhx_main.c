#include "util.h"
#include "approximation_level.h"
#include "approximation.h"
#include "input.h"


int printf(const char* fmt, ...); // from common/syscalls.c
void setStats(int);               // from common/util.h


// Reuse maximum computations than redoing.
#define  SIZE  100

double global_fact = 1;
double global_num  = 1;
const int terms = SIZE;


double factorial_array[SIZE + 1] = {0};
double factorial (double num) {
  if (num == 0) {
    global_num = 1;
    global_fact = 1;
    return global_fact;
  }
  if (global_num == (num - 1)) {
    global_fact = global_fact * num;
    global_num = num;
    return global_fact;
  }
  if (global_num == (num - 2)) {
    global_fact = global_fact * (num-1) * num;
    global_num = num;
    return global_fact;
  }

  
  global_num = num;
  double i;
  double fact = 1;
  for (i=1; i<=num; i++) {
    fact = fact*i;
  }
  global_fact = fact;
  return fact;
}


double power_normal (double xx, int yy) {
  if (yy == 0) return 1.0;
  if (yy == 1) return xx;
  if (xx == 1) return 1.0;
  if (xx == 0) return 0.0;

  int i;
  double res = 1.0;
  for (i=1; i<= yy; i++) {
    res = res * xx;
  }

  return res;
}



double global_xx = 0.0;
double global_yy = 100.0;
double power_parray[SIZE + 1] = {0};
double power_marray[SIZE + 1] = {0};
double power (double xx, double yy) {
  if (yy == 0) return 1;
  if (global_xx == 0.0) { global_xx = xx; }
  if (global_yy == (yy - 1)) {
   global_xx = global_xx * xx;
   global_yy = yy;
   return global_xx;
  }


  if (global_yy == (yy - 2)) {
   global_xx = global_xx * xx;
   global_xx = global_xx * xx;
   global_yy = yy;
   return global_xx;
  }

  
  int i=1;
  global_xx = 1;
  for (i=1; i<=yy; i++) {
    global_xx = global_xx * xx;
  }
  global_yy = yy;
  return global_xx;

}


double etox (double x, int pos) {
// 1+x+x^2/2!+x^3/3!
  int i;
  double sum = 0;
  double p, f;

  for (i=0; i<terms; i++) {
    //double p = power (x, i);
    //double f = factorial (i);
    if (1 == pos) p = power_parray[i];
    else p = power_marray[i];
    double f = factorial_array[i];
   
    double sign = 1;
    if (p < 0) { 
      p = p * -1;
      sign = -1;
    }
    double res = p / f;
    sum = sum + (sign * res);
  }

  return sum;
}


double tanhx (double x) {
  // (e^x - e^-x) / 2
  
  double eppx = etox (x, 1);
  double epmx = etox (-x, -1);
  double nr = eppx - epmx;
  double dr = eppx + epmx;
  double result = nr / dr;
  return result;
}




int main () {

  int i;
  double number = _NUMBER_;
  
  double p, f;
  // pre-compute the factorials and powers
  for (i=0; i<terms; i++) {
    p = power_normal (number, i);
    power_parray[i] = p;
    p = power_normal (-number, i);
    power_marray[i] = p;
    f = factorial (i);
    factorial_array[i] = f;
  }

  //set_approximation_level (APPROX_LEVEL, 0);
  set_approximation_level (APPROX_LEVEL, EXP_LEVEL);
  //set_approximation_level (-1, 0);
  setStats(1);
  double result = tanhx (number);
  setStats(0);
  
  long mult = 1000000000; // precision of x 10^10, i.e 10 digits after decimal point.
  printf ("tanhx of %ld x 10^(-9)  =  %ld x 10^(-9)  [ approx = %d ]\n", 
	  (long)(number * mult), (long)(result * mult), APPROX_LEVEL);

  return 0;
}
