#=======================================================================
# UCB CS250 Makefile fragment for benchmarks
#-----------------------------------------------------------------------
#
# Each benchmark directory should have its own fragment which
# essentially lists what the source files are and how to link them
# into an riscv and/or host executable. All variables should include
# the benchmark name as a prefix so that they are unique.
#

tanhx_c_src = \
	tanhx_main.c \
	syscalls.c \

tanhx_riscv_src = \
	crt.S \

tanhx_c_objs     = $(patsubst %.c, %.o, $(tanhx_c_src))
tanhx_riscv_objs = $(patsubst %.S, %.o, $(tanhx_riscv_src))

tanhx_host_bin = tanhx.host
$(tanhx_host_bin) : $(tanhx_c_src)
	$(HOST_COMP) $^ -o $(tanhx_host_bin)

tanhx_riscv_bin = tanhx.riscv
$(tanhx_riscv_bin) : $(tanhx_c_objs) $(tanhx_riscv_objs)
	$(RISCV_LINK) $(tanhx_c_objs) $(tanhx_riscv_objs) -o $(tanhx_riscv_bin) $(RISCV_LINK_OPTS)

junk += $(tanhx_c_objs) $(tanhx_riscv_objs) \
        $(tanhx_host_bin) $(tanhx_riscv_bin)
