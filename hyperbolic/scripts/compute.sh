#!/usr/bin/env bash
#
# Usage : compute.sh <function> <value> <approximation-level>
#
#  e.g. : compute.sh sinhx  0.5 -1  # approximations completely disabled.
#       : compute.sh sinhx  0.5 20  # approximation enabled and set at level 20
#       : compute.sh atanhx 1.5 20  # approximation enabled and set at level 20
#
# Result will be output to the host terminal.
#-------------------------------------------------------------------------------

print_usage () {
    echo "Usage: compute.sh <function> <value> <approximation-level>";
    echo "e.g. : compute.sh sinhx  0.5 -1  # approximations completely disabled."
    echo "       compute.sh sinhx  0.5 20  # approximation enabled and set at level 20"
    echo "       compute.sh atanhx 1.5 20  # approximation enabled and set at level 20"
    echo "       "
    echo "       Available functions : sinhx, coshx, tanhx, asinhx, acoshx, atanhx"
}

if [ -z $1 ]; then echo "Error: function not provided!"; print_usage; exit; fi
if [ -z $2 ]; then echo "Error: value not provided!"; print_usage; exit; fi
if [ -z $3 ]; then echo "Error: approximation-level not provided!"; print_usage; exit; fi


fx=$1
val=$2
appx=$3
#-------------------------------------------------------------------------------
command -v sshpass >/dev/null 2>&1 || { echo >&2 "Program sshpass is not installed. Either install sshpass or copy all these files manually.  Aborting."; exit 1; }
command -v scp >/dev/null 2>&1 || { echo >&2 "Program scp is not installed. Must have scp and ssh to communicate with zedboard.  Aborting."; exit 1; }

copy_to_zedboard () {
    sshpass -p 'root' scp $1 root@192.168.1.5:~/
}

copy_from_zedboard () {
    sshpass -p 'root' scp root@192.168.1.5:~/${1} .    
}

run_in_zedboard () {
    sshpass -p 'root' ssh root@192.168.1.5 -t ./exec_zedboard.sh $1 $2
}
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------


# 1. Update the function in Makefile
sed -i -e  "s/bmarks =.*/bmarks = ${fx}/g" Makefile
# 2. Update the value 
sed -i -e "s/_NUMBER_.*/_NUMBER_  ${val}/g" ${fx}/input.h
# 3. Update the approximation level.
sed -i -e "s/APPROX_LEVEL.*/APPROX_LEVEL ${appx}/g"  ${fx}/approximation_level.h
# 4. Generate the riscv binaries
make clean && make riscv
# 5. copy the binaries to zedboard
copy_to_zedboard ${fx}.riscv
# 6. Also copy a convenience script for execution in zedboard.
copy_to_zedboard ./scripts/exec_zedboard.sh 
# 7. run the program in zedboard
run_in_zedboard ${fx}.riscv ${fx}_val${val}_appx${appx}.rpt
# 8. copy the result back to host.
copy_from_zedboard ${fx}_val${val}_appx${appx}.rpt
# 9. And display.
echo  
echo "RESULTS :: "
echo  
cat ${fx}_val${val}_appx${appx}.rpt


#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
