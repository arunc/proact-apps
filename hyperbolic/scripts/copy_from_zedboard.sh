#!/usr/bin/env bash

command -v sshpass >/dev/null 2>&1 || { echo >&2 "Program sshpass is not installed. Either install sshpass or copy all these files manually.  Aborting."; exit 1; }
command -v scp >/dev/null 2>&1 || { echo >&2 "Program scp is not installed. Must have scp and ssh to communicate with zedboard.  Aborting."; exit 1; }

if [ -z $1 ]; then echo "Usage: copy_from_zedboard.sh item"; exit;fi

sshpass -p 'root' scp root@192.168.1.5:~/${1} .

