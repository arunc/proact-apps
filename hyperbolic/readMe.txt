Hyperbolic functions - with and without approximations

IMPORTANT:
FIRST you must clone git@gitlab.com:arunc/proact-processor.git  
SECOND, build the RISC-V tools as in proact-processor/build/HowToBuild.txt, and 
THIRD, set the RISC-V environment variable as in proact-processor/set_env.sh
This project (proact-processor) is independent of proact-apps

Follow the steps to create a proact application for edge-detection, run it in zedboard, and 
Follow the steps to create a proact application for hyperbolic functions, run it in zedboard, and 
compare the speed improvements when approximations are turned on.

----------------------------------------------------------------------------------------------------
1) # BOOT UP THE ZEDBOARD. Verify by ping 192.168.1.5
2) cd ..; ./utils/prepare_zedboard.sh; cd -  # Setup the front-end server 

Now for approximations disabled:
3) ./scripts/compute.sh sinhx 1.5 -1  # <function> <value> <approx-level>
  # -1 means approximations completely disabled.
  # Generates the riscv application program. copies it to zedboard, executes it there and copies result back.
  # And will print the results to host screen.
  # Results also available at sinhx_val1.5_appx-1.rpt ( ${function}_val{value}_appx${approx-level}.rpt )  

Now for approximations enabled:
4) ./scripts/compute.sh sinhx 1.5 20  # <function> <value> <approx-level>
  # 20 means approximations enabled and set at a value of 20.
  # Generates the riscv application program. copies it to zedboard, executes it there and copies result back.
  # And will print the results to host screen. 
  # Results also available at sinhx_val1.5_appx20.rpt ( ${function}_val{value}_appx${approx-level}.rpt )  


**** COMPARE THE RESULTS sinhx_val1.5_appx-1.rpt VS sinhx_val1.5_appx20.rpt
**** AND CHECK THE MCYCLE entry (the number of machine cycles taken for computations)
**** Should see a considerable difference there.

You may play around with the functions, values and approx-level.
Available functions are : sinhx, coshx, tanhx, asinhx, acoshx, atanhx

Login to Zedboard (ssh root@192.168.1.5) (password root), and see and run the programs manually.

Go through ./script/compute.sh for each step
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
