#=======================================================================
# UCB CS250 Makefile fragment for benchmarks
#-----------------------------------------------------------------------
#
# Each benchmark directory should have its own fragment which
# essentially lists what the source files are and how to link them
# into an riscv and/or host executable. All variables should include
# the benchmark name as a prefix so that they are unique.
#

acoshx_c_src = \
	acoshx_main.c \
	syscalls.c \

acoshx_riscv_src = \
	crt.S \

acoshx_c_objs     = $(patsubst %.c, %.o, $(acoshx_c_src))
acoshx_riscv_objs = $(patsubst %.S, %.o, $(acoshx_riscv_src))

acoshx_host_bin = acoshx.host
$(acoshx_host_bin) : $(acoshx_c_src)
	$(HOST_COMP) $^ -o $(acoshx_host_bin)

acoshx_riscv_bin = acoshx.riscv
$(acoshx_riscv_bin) : $(acoshx_c_objs) $(acoshx_riscv_objs)
	$(RISCV_LINK) $(acoshx_c_objs) $(acoshx_riscv_objs) -o $(acoshx_riscv_bin) $(RISCV_LINK_OPTS)

junk += $(acoshx_c_objs) $(acoshx_riscv_objs) \
        $(acoshx_host_bin) $(acoshx_riscv_bin)
