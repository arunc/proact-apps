#include "util.h"
#include "approximation_level.h"
#include "approximation.h"
#include "input.h"


int printf(const char* fmt, ...); // from common/syscalls.c
void setStats(int);               // from common/util.h


// Reuse maximum computations than redoing.
#define  SIZE  100

double global_fact = 1;
double global_num  = 1;
const int terms = SIZE;


double factorial_array[SIZE + 1] = {0};
double factorial (double num) {
  if (num == 0) {
    global_num = 1;
    global_fact = 1;
    return global_fact;
  }
  if (global_num == (num - 1)) {
    global_fact = global_fact * num;
    global_num = num;
    return global_fact;
  }
  if (global_num == (num - 2)) {
    global_fact = global_fact * (num-1) * num;
    global_num = num;
    return global_fact;
  }

  
  global_num = num;
  double i;
  double fact = 1;
  for (i=1; i<=num; i++) {
    fact = fact*i;
  }
  global_fact = fact;
  return fact;
}


double power_normal (double xx, int yy) {
  if (yy == 0) return 1.0;
  if (yy == 1) return xx;
  if (xx == 1) return 1.0;
  if (xx == 0) return 0.0;

  int i;
  double res = 1.0;
  for (i=1; i<= yy; i++) {
    res = res * xx;
  }

  return res;
}



double global_xx = 0.0;
double global_yy = 100.0;
double power_parray[SIZE + 1] = {0};
double power_marray[SIZE + 1] = {0};
double power (double xx, double yy) {
  if (yy == 0) return 1;
  if (global_xx == 0.0) { global_xx = xx; }
  if (global_yy == (yy - 1)) {
   global_xx = global_xx * xx;
   global_yy = yy;
   return global_xx;
  }


  if (global_yy == (yy - 2)) {
   global_xx = global_xx * xx;
   global_xx = global_xx * xx;
   global_yy = yy;
   return global_xx;
  }

  
  int i=1;
  global_xx = 1;
  for (i=1; i<=yy; i++) {
    global_xx = global_xx * xx;
  }
  global_yy = yy;
  return global_xx;

}




double etox (double x) {
// 1+x+x^2/2!+x^3/3!
  int i;
  double sum = 0;
  double p, f;

  for (i=0; i<terms; i++) {
    double p = power_normal (x, i);
    double f = factorial_array[i];
   
    double sign = 1;
    if (p < 0) { 
      p = p * -1;
      sign = -1;
    }
    double res = p / f;
    sum = sum + (sign * res);
  }

  return sum;
}


double y_array [SIZE + 1] = {0};
double xfactor = 0.0;
double y2_minus_1 = 0.0;
// cases less than 100 only considered here. Remaining ifs ignored for time being.
double log1plusy () {
  double y = y2_minus_1;
  // ln (1+y) = y - (y2/2) + (y3/3) - (y4/4) + ...
  // 1+y = x; y => x-1
  int i = 0;
  double sign = -1;
  double sum = y;
  for (i = 2; i<terms-1; i++) { // 98 terms, debug crash.
    double yp = y_array [i];
    double temp =  ( (sign) * (yp / (double) i) );
    //printf ("yp = %ld x 10^-5   i = %ld x 10^-5    temp = %ld x 10^-5 \n", 
    //(long)(100000 * yp), (long)(100000 * i), (long)(100000 * temp) );
    sum = sum + temp; 
    //printf ("sum = %ld x 10^-5 \n", (long)(100000 * sum)  );
    if (sign == -1) sign  = 1;
    else sign = -1;
  }

  return sum + xfactor;
}


// loge (n); n < 100. Above 100 not considered for the timebeing.
double log_normal (double yy) {
  double factor = 0.0;
  if ( yy > 1 && yy < 10) { factor = 2.302585093; yy = yy / 10.0; }
  if ( yy >= 10 && yy < 100) { factor = 4.605170186; yy = yy / 100.0; }
  double y = yy - 1;
  
  // ln (1+y) = y - (y2/2) + (y3/3) - (y4/4) + ...
  // 1+y = x; y => x-1
  int i = 0;
  double sign = -1;
  double sum = y;
  for (i = 2; i<terms-1; i++) {
    double yp = power_normal (y, i);
    sum = sum +  ( (sign) * (yp / (double) i) ); 
    if (sign == -1) sign  = 1;
    else sign = -1;
  }
  return sum + factor;
}


double acoshx (double x) {
  // acoshx (x) = ln ( x + sqrt (x2 + 1) )
  // sqrt (p) = e^( log(p) / 2 )
  
  //double lnp = log1plusy (p + 1);
  double lnp = log1plusy ();
  double sqrtp = etox ( lnp / (double)2.0 );
  double result_p_1 = ( x + sqrtp );
  double result = log_normal ( result_p_1);
  return result;
}




int main () {

  int i;
  double number = _NUMBER_;
  
  // pre-compute the factorials and powers
  // works only if y2_minus_1 is < 100. Remaining multipliers simply not provided.
  y2_minus_1 = (number * number) - 1 ;
  if ( y2_minus_1 > 1 && y2_minus_1 < 10) { 
    xfactor = 2.302585093;  
    y2_minus_1 = y2_minus_1 / 10.0; 
  }
  if ( y2_minus_1 >= 10 && y2_minus_1 < 100) { 
    xfactor = 4.605170186;  
    y2_minus_1 = y2_minus_1 / 100.0; 
  }
  y2_minus_1 = y2_minus_1 - 1;

  double f, yp;
  for (i=0; i<terms; i++) {
    yp = power_normal (y2_minus_1, i);
    y_array[i] = yp;
    f = factorial (i);
    factorial_array[i] = f;
  }

  set_approximation_level (APPROX_LEVEL, EXP_LEVEL);
  setStats(1);
  double result = acoshx (number);
  setStats(0);
  
  long mult = 1000000000; // precision of x 10^10, i.e 10 digits after decimal point.
  printf ("acoshx of %ld x 10^(-9)  =  %ld x 10^(-9)  [ approx = %d ]\n", 
	  (long)(number * mult), (long)(result * mult), APPROX_LEVEL);

  return 0;
}
