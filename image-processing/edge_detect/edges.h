#pragma once
#ifndef EDGES_H
#define EDGES_H

#ifdef __x86_64__
#include <stdio.h>
#endif

#ifdef __x86_32__
#include <stdio.h>
#endif

#include <image_size.h>

void contrast_edge_detect (double  the_image[HEIGHT][WIDTH], double out_image[HEIGHT][WIDTH]);

#endif


