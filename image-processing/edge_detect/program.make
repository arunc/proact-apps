edge_detect_c_src = \
	edge_detect.c \
	edges.c \
	syscalls.c \

edge_detect_riscv_src = \
	crt.S \

edge_detect_c_objs     = $(patsubst %.c, %.o, $(edge_detect_c_src))
edge_detect_riscv_objs = $(patsubst %.S, %.o, $(edge_detect_riscv_src))

edge_detect_host_bin = edge_detect.host
$(edge_detect_host_bin) : $(edge_detect_c_src)
	$(HOST_COMP) $^ -o $(edge_detect_host_bin)

edge_detect_riscv_bin = edge_detect.riscv
$(edge_detect_riscv_bin) : $(edge_detect_c_objs) $(edge_detect_riscv_objs)
	$(RISCV_LINK) $(edge_detect_c_objs) $(edge_detect_riscv_objs) -o $(edge_detect_riscv_bin) $(RISCV_LINK_OPTS)

junk += $(edge_detect_c_objs) $(edge_detect_riscv_objs) \
        $(edge_detect_host_bin) $(edge_detect_riscv_bin)



#---edge_detect_c_src = edge_detect.c edges.c syscalls.c
#---
#---edge_detect_c_objs     = $(patsubst %.c, %.o, $(edge_detect_c_src))
#---
#---edge_detect_riscv_bin = edge_detect.riscv
#---$(edge_detect_riscv_bin) : $(edge_detect_c_objs) 
#---	$(RISCV_LINK) $(edge_detect_c_objs)  -o $(edge_detect_riscv_bin) $(RISCV_LINK_OPTS)
#---
#---junk += $(edge_detect_c_objs) \
#---        $(edge_detect_host_bin) $(edge_detect_riscv_bin)
