// 
// read the notes in common.h for some important considerations.
//
#include "edges.h"


// everything declared global to allocate in .data (stack is limited)
// ce_ prefix denotes contrast-edge

const double constrast_mask[3][3] = {
  {-0.1111111, 100000000, -0.1111111},
  { 100000000, 0.0277777,  100000000},
  {-0.1111111, 100000000, -0.1111111} 
};


double ce_d;
double ce_sum_d, ce_sum_n;
const double ce_max = 255;
  
// this is pass by reference.
// still picks the values from the global array (.data) and not local (.stack)
void contrast_edge_detect (double  in_image[HEIGHT][WIDTH], double out_image[HEIGHT][WIDTH]) {

  // picks rows, cols, bits_per_pixel
  const long rows = HEIGHT;
  const long cols = WIDTH;
  
  int x, y; // indices
  int i, j; // loop vars
  
  for (i=1; i<rows-1; i++) { // main iter
    for (j=1; j<cols-1; j++) {
      ce_sum_n = 0; ce_sum_d = 0;
      
      for (x=-1; x<2; x++) {
	for (y=-1; y<2; y++) {
	  ce_sum_n = ce_sum_n + in_image[i+x][j+y] /  constrast_mask[x+1][y+1];
	  ce_sum_d = ce_sum_d + in_image[i+x][j+y];
	}
      }
      
      ce_d = ce_sum_d / 9.0;
      if (ce_d == 0) ce_d = 1.0;
      
      out_image[i][j] = ce_sum_n/ce_d;
      
      if (out_image[i][j] > ce_max)   out_image[i][j] = ce_max;
      if (out_image[i][j] < 0)     out_image[i][j] = 0;
    }
  }  // end of main iter.
  
} 

