
#include <common_riscv/common.h>
#include <image_size.h>
#include <edges.h>
#include <in_image.h>
#include <common_riscv/approximation.h>
#include <approximation_level.h>

static double out_image[HEIGHT][WIDTH] = {0};

int main (int argc, char *argv[]) {
  int   i, j;
   
  const long height = HEIGHT; const long width = WIDTH; 
  const long bits_per_pixel = 8;
  const int high = 0;   // don't change this values.
  const int threshold = 1; // if needed change threshold to 0.

  printf ("[info] start\n");

#ifdef __RISCV_64__
  set_approximation_level (APPROX_LEVEL, EXP_LEVEL);
  setStats(1);
#endif
  contrast_edge_detect (in_image, out_image);

#ifdef __RISCV_64__
  setStats(0);
#endif

  // post processing with a threshold.
  if (1 == threshold) {
    double new_hi  = 250;
    double new_low = 16;
    //double new_hi  = 255;
    //double new_low = 0;
    for (i=0; i<height; i++) {   // threshold the output image.
      for (j=0; j<width; j++) {
  	if (out_image[i][j] > high) out_image[i][j] = new_hi;
  	else out_image[i][j] = new_low;
      }
    }
  }


  printf ("[info] finish\n");

  for (i=0; i<height; i++) {
    for (j=0; j<width; j++)
      printf ("%ld\n", (long)out_image[i][j]);
  }

  return 0;
} 


