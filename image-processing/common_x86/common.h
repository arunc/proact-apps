#pragma once
#ifndef COMMON_H
#define COMMON_H

//-----------------------------------------------------------------------------------
/* 
Notes:

In C, variables are allocated as below:
http://stackoverflow.com/questions/14588767/where-in-memory-are-my-variables-stored-in-c

global vars -> data 
static vars -> data 
constant data -> code and/or data. 
local vars (declared and defined in functions) -> stack
vars declared and defined in main function -> stack 
pointers (int *arr) -> data or stack
dynamically allocated (malloc) -> heap (In RISCV emulator, not available. Never use.)

And our STACK is very very limited. Limited to some 20000 bytes.
Hence if the images (512 x 512 big) are provided as local variables (i.e. allocated in 
the stack), these will corrupt code-segment and the program will show UB.
Hence it is extremely important to move everything out to global scope.
This is the strategy used in these programs.

Also in calls like    edge_detect (in_image[][], out_image[][])
the arrays are always passed as reference (but through a copy of the pointer, which is OK).

 */
//-----------------------------------------------------------------------------------

// 64 bit x86
#ifdef __x86_64__
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
// 32 bit x86
#elif __x86_32__
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
// 64 bit RISCV
#else 
int printf(const char* fmt, ...); // from common/syscalls.c
void setStats(int);               // from common/util.h
#  ifndef __RISCV_64__
#  define __RISCV_64__
#  endif
#endif

//-----------------------------------------------------------------------------------

#define GRAY_LEVELS          255

struct bmpfileheader {
  unsigned short  filetype;
  unsigned long   filesize;
  short  reserved1;
  short  reserved2;
  unsigned long   bitmapoffset;
};

struct bitmapheader {
  unsigned long size;
  long   width;
  long   height;
  unsigned short  planes;
  unsigned short  bitsperpixel;
  unsigned long   compression;
  unsigned long   sizeofbitmap;
  unsigned long   horzres;
  unsigned long   vertres;
  unsigned long   colorsused;
  unsigned long   colorsimp;
};

struct ctstruct {
  unsigned char blue;
  unsigned char green;
  unsigned char red;
};




union short_char_union {
  short s_num;
  char  s_alpha[2];
};

union int_char_union {
  int  i_num;
  char i_alpha[2];
};

union long_char_union {
  long  l_num;
  char  l_alpha[4];
};

union float_char_union {
  float f_num;
  char  f_alpha[4];
};

union ushort_char_union {
  short s_num;
  char  s_alpha[2];
};

union uint_char_union {
  int  i_num;
  char i_alpha[2];
};

union ulong_char_union {
  long  l_num;
  char  l_alpha[4];
};



#endif
