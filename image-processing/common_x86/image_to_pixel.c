// Utility takes in a 8 bits per pixel black and white BMP image.
// dumps out the values in a C 2-D array form.
//
//
// Usage : ./image_to_pixel.exe <input-image.bmp> <output-pixel.c> <height> <width>
//         anything else is seg fault
//-------------------------------------------------------------------------  


#include <stdio.h>
#include <assert.h>
#include <common.h>
#include <imageio.h>
#include <stdlib.h>  // for strtol

int main (int argc, char *argv[]) {

  char  image_name [1024*2]; // avoid buffer overflow. costs nothing.
  char  txt_name [1024*2];   // avoid buffer overflow. costs nothing.
  
  int   i, j;
  short **the_image;
  
  assert (argv[1] != NULL && "Input BMP image file is NOT provided!" );
  assert (argv[2] != NULL && "Output pixel file is NOT provided!" );
  assert (argv[3] != NULL && "Height of input BMP image NOT provided!" );
  assert (argv[4] != NULL && "Width of input BMP image NOT provided!" );

  strcpy (image_name,  argv[1]);
  strcpy (txt_name, argv[2]);
  
  long height = strtol(argv[3], NULL, 10); // 10 is the base of conversion.
  long width  = strtol(argv[4], NULL, 10); // 10 is the base of conversion.
  const long bits_per_pixel = 8;

  the_image = (short **) malloc (height * sizeof(short  *));
  for(i=0; i<width; i++){
    the_image[i] = (short *) malloc (width * sizeof(short ));
    assert ((the_image[i] != NULL) && "[ERROR] malloc failed!"); 
   }

  read_bmp_image (image_name, the_image);
  
  // write the pixel file
  FILE *fp;
  fp = fopen (txt_name, "w");
  assert (fp != NULL);
  
  fprintf (fp, "// --- Image pixel C source file for %s \n", txt_name);
  //fprintf (fp, "// --- Function setup_image() returns image array in double **.\n\n");
  fprintf (fp, "#ifndef _FULL_IMAGE_PIXELS_ \n#define _FULL_IMAGE_PIXELS_  \n");
  //fprintf (fp, "double [][] setup_image () { \n");
  fprintf (fp, "   double in_image[%ld][%ld] = { \n", height, width);
  
  for (i=0; i<height;i++) {
    fprintf (fp, "          {");     
    for (j=0; j<width-1;j++) {
      fprintf (fp, "%d.000,  ", the_image[i][j]);
    }
    fprintf (fp, "%d.000", the_image[i][width-1]);
    
    if (i == height-1) 
      fprintf (fp,    "}\n");     
    else 
      fprintf (fp,    "},\n");     
  }

  fprintf (fp, "    }; \n\n");     
  //fprintf (fp, "return image; \n} \n\n");     
  fprintf (fp, "#endif \n\n");
  fclose (fp);




  // copy height and width to image_size.h
  FILE *fpp = fopen ("image_size.h", "w");
  assert (fpp != NULL);
  fprintf (fpp, "// This header is written by image_to_pixel.exe. Do NOT modify unless you know what you are doing. :) \n");
  fprintf (fpp, "#ifndef IMAGE_SIZE_H \n");
  fprintf (fpp, "#define IMAGE_SIZE_H \n");
  fprintf (fpp, "\n#define HEIGHT %ld      // height of image %s \n", height, image_name);
  fprintf (fpp, "#define WIDTH  %ld      // width  of image %s \n", width,  image_name);
  fprintf (fpp, "\n\n#endif \n");
  fclose (fpp);


  
  for (i=0; i<height; i++) // avoid memory leak
    free (the_image[i]);

  return 0;
  
}
