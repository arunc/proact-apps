// Helper functions for BMP image manipulation. Mainly I/O functions 
// read_bmp_image () and write_bmp_image()
// 
// Original functions by D Philip.
// Many of the functions are almost line-by-line copy of the original.

#include "imageio.h"


int is_little_endian () {
  int n = 1;
  if ( *(char*)&n == 1 ) return 1;
  else return 0;
}

// takes a four byte long out of a buffer of characters.
void extract_long_from_buffer (char buffer[], int lsb, int start, long *number) {
  if ( is_little_endian() ) {
    *number = buffer[0 + start] | (buffer[1+start] << 8) | 
      (buffer[2+start] << 16) | (buffer[3+start] << 24);
  } else {
    assert (0 && "BIG ENDIAN. Need to think for extract_long..()");
  }
  
  return;
}


void extract_ulong_from_buffer (char buffer[], int lsb, int start, unsigned long * number) {
  if ( is_little_endian() ) {
    *number = buffer[0 + start] | (buffer[1+start] << 8) | 
      (buffer[2+start] << 16) | (buffer[3+start] << 24);
  } else {
    assert (0 && "BIG ENDIAN. Need to think for extract_ulong..()");
  }
  
  return;
} 

void extract_short_from_buffer (char buffer[], int lsb, int start, short *number) {
  if ( is_little_endian() ) {
    *number = buffer[0 + start] | (buffer[1+start] << 8);
  } else {
    assert (0 && "BIG ENDIAN. Need to think for extract_short...()");
  }

  return;
}

void extract_ushort_from_buffer (char  buffer[], int lsb, int start, unsigned short *number )
{
  if ( is_little_endian() ) {
    *number = buffer[0 + start] | (buffer[1+start] << 8);
  } else {
    assert (0 && "BIG ENDIAN. Need to think for extract_short...()");
  }

  return;
}

// insert a two byte short into a  buffer of characters in LSB order.
void insert_short_into_buffer ( char  buffer[], int   start, short number) {
    union short_char_union lsu;

    lsu.s_num       = number;
    buffer[start+0] = lsu.s_alpha[0];
    buffer[start+1] = lsu.s_alpha[1];
}  

// insert a two byte unsigned short into a buffer of characters in LSB order
void insert_ushort_into_buffer ( char  buffer[], int   start, unsigned short number)
{
    union ushort_char_union lsu;

    lsu.s_num       = number;
    buffer[start+0] = lsu.s_alpha[0];
    buffer[start+1] = lsu.s_alpha[1];

}

// insert a four byte long into a buffer of characters in LSB order.
void insert_long_into_buffer ( char buffer[], int  start, long number) {
    union long_char_union lsu;

    lsu.l_num       = number;
    buffer[start+0] = lsu.l_alpha[0];
    buffer[start+1] = lsu.l_alpha[1];
    buffer[start+2] = lsu.l_alpha[2];
    buffer[start+3] = lsu.l_alpha[3];

}

// insert a four byte unsigned long into a buffer of characters in LSB order
void insert_ulong_into_buffer ( char buffer[], int  start, unsigned long number) {
    union ulong_char_union lsu;

    lsu.l_num       = number;
    buffer[start+0] = lsu.l_alpha[0];
    buffer[start+1] = lsu.l_alpha[1];
    buffer[start+2] = lsu.l_alpha[2];
    buffer[start+3] = lsu.l_alpha[3];

}

// reads bmpfileheader info from a bmp image file.
void read_bmp_file_header ( char *file_name, struct bmpfileheader *file_header) {
  char  buffer[10];
  long  ll;
  short ss;
  unsigned long  ull;
  unsigned short uss;
  FILE     *fp;
  
  fp = fopen(file_name, "rb");

  fread(buffer, 1, 2, fp);
  extract_ushort_from_buffer(buffer, 1, 0, &uss);
  file_header->filetype = uss;
  
  fread(buffer, 1, 4, fp);
  extract_ulong_from_buffer(buffer, 1, 0, &ull);
  file_header->filesize = ull;
  
  fread(buffer, 1, 2, fp);
  extract_short_from_buffer(buffer, 1, 0, &ss);
  file_header->reserved1 = ss;
  
  fread(buffer, 1, 2, fp);
  extract_short_from_buffer(buffer, 1, 0, &ss);
  file_header->reserved2 = ss;
  
  fread(buffer, 1, 4, fp);
  extract_ulong_from_buffer(buffer, 1, 0, &ull);
  file_header->bitmapoffset = ull;
  
  fclose(fp);
}


// reads bitmapheader structure from bmp image file.
void read_bm_header ( char *file_name, struct bitmapheader *bmheader) {
  char  buffer[10];
  long  ll;
  short ss;
  unsigned long  ull;
  unsigned short uss;
  FILE *fp;
  
  fp = fopen(file_name, "rb");
  
  /****************************************
   *
   *   Seek past the first 14 byte header.
   *
   ****************************************/
  
  fseek(fp, 14, SEEK_SET);
  
  
  fread(buffer, 1, 4, fp);
  extract_ulong_from_buffer(buffer, 1, 0, &ull);
  bmheader->size = ull;
  
  //printf ("[i] DIB Header Size = %ld \n", ull);
  
  fread(buffer, 1, 4, fp);
  extract_long_from_buffer(buffer, 1, 0, &ll);
  if (ll < 0) ll = ll * -1;
  bmheader->width = ll;
  
  fread(buffer, 1, 4, fp);
  extract_long_from_buffer(buffer, 1, 0, &ll);
  if (ll < 0) ll = ll * -1;
  bmheader->height = ll;
  
  fread(buffer, 1, 2, fp);
  extract_ushort_from_buffer(buffer, 1, 0, &uss);
  bmheader->planes = uss;
  
  fread(buffer, 1, 2, fp);
  extract_ushort_from_buffer(buffer, 1, 0, &uss);
  bmheader->bitsperpixel = uss;
  
  fread(buffer, 1, 4, fp);
  extract_ulong_from_buffer(buffer, 1, 0, &ull);
  bmheader->compression = ull;
  
  fread(buffer, 1, 4, fp);
  extract_ulong_from_buffer(buffer, 1, 0, &ull);
  bmheader->sizeofbitmap = ull;
  
  fread(buffer, 1, 4, fp);
  extract_ulong_from_buffer(buffer, 1, 0, &ull);
  bmheader->horzres = ull;
  
  fread(buffer, 1, 4, fp);
  extract_ulong_from_buffer(buffer, 1, 0, &ull);
  bmheader->vertres = ull;
  
  fread(buffer, 1, 4, fp);
  extract_ulong_from_buffer(buffer, 1, 0, &ull);
  bmheader->colorsused = ull;
  
  fread(buffer, 1, 4, fp);
  extract_ulong_from_buffer(buffer, 1, 0, &ull);
  bmheader->colorsimp = ull;
  
  fclose(fp);
  
}

// read color table from bmp image.
void read_color_table ( char   *file_name, struct ctstruct *rgb, int size) {
   int  i;
   char buffer[10];
   FILE *fp;

   fp = fopen (file_name, "rb");

   fseek (fp, 54, SEEK_SET);
   for (i=0; i<size; i++) {
     fread(buffer, 1, 1, fp);
     rgb[i].blue = buffer[0];
     fread(buffer, 1, 1, fp);
     rgb[i].green = buffer[0];
     fread(buffer, 1, 1, fp);
     rgb[i].red = buffer[0];
     fread(buffer, 1, 1, fp);
     /* fourth byte nothing */
   }  

   fclose(fp);
}

// flip image array about its horizontal mid-point.
void flip_image_array ( short **the_image, long   cols, long rows ) {
   int  i, j;
   short **temp;

   temp = (short **) malloc (rows * sizeof(short  *));
   for (i=0; i<cols; i++) {
     temp[i] = (short *)malloc (cols * sizeof(short ));
     assert ( (temp[i] != NULL) && "flip_image_array() malloc failed!");
   } 

   long rd2  = rows/2;
   for (i=0; i<rd2; i++)
     for (j=0; j<cols; j++)
       temp[rows-1-i][j] = the_image[i][j];

   for (i=rd2; i<rows; i++)
     for (j=0; j<cols; j++)
       temp[rows-1-i][j] = the_image[i][j];

   for (i=0; i<rows; i++)
     for (j=0; j<cols; j++)
       the_image[i][j] = temp[i][j];

   for (i=0; i<rows; i++)
     free(temp[i]);

}  


// read pixel array from bmp image. ONLY 8-bit per pixel
void read_bmp_image ( char  *file_name,  short **array) {
  FILE   *fp;
  int    i, j;
  int    negative = 0, pad = 0, place = 0;
  long   colors = 0, height = 0, position = 0, width = 0;
  struct bmpfileheader file_header;
  struct bitmapheader  bmheader;
  struct ctstruct rgb[GRAY_LEVELS+1];
  unsigned char uc;

  read_bmp_file_header(file_name, &file_header);
  read_bm_header(file_name, &bmheader);
  assert ( (bmheader.bitsperpixel == 8) && "read_bmp_image() bits-per-pixel is NOT 8" );
   
  if (0 == bmheader.colorsused) colors = GRAY_LEVELS + 1;
  else  colors = bmheader.colorsused;
  read_color_table (file_name, rgb, colors);

  fp = fopen (file_name, "rb");
  fseek (fp, file_header.bitmapoffset, SEEK_SET);
  
  width = bmheader.width;
  if (width < 0) width = width * -1;
  if (bmheader.height < 0) {
    height   = bmheader.height * (-1);
    negative = 1;
  } else {
    height = bmheader.height;
  }
    
  pad = calculate_pad(width);
  printf ("height = %ld   width = %ld  pad = %d \n", height, width, pad);

  for (i=0; i<height; i++) {
    for (j=0; j<width; j++) {
      place = fgetc(fp);
      uc = (place & 0xff);
      place = uc;
      array[i][j] = rgb[place].blue;
    }
    if (pad != 0)  position = fseek (fp, pad, SEEK_CUR);
  }  
  
  if(negative == 0) flip_image_array(array, height, width);

}  

// setup the bmp file except the height and width parameters.
void create_allocate_bmp_file ( char  *file_name,
				struct bmpfileheader *file_header,
				struct bitmapheader  *bmheader) {
   char buffer[100];
   int  i, pad = 0;
   FILE *fp;

   pad = calculate_pad(bmheader->width);

   bmheader->size         =  40;
   bmheader->planes       =   1;
   bmheader->bitsperpixel =   8;
   bmheader->compression  =   0;
   bmheader->sizeofbitmap = bmheader->height * 
                            (bmheader->width + pad);
   bmheader->horzres      = 300;
   bmheader->vertres      = 300;
   bmheader->colorsused   = 256;
   bmheader->colorsimp    = 256;

   file_header->filetype     = 0x4D42;
   file_header->reserved1    =  0;
   file_header->reserved2    =  0;
   file_header->bitmapoffset = 14 + 
                               bmheader->size +
                               bmheader->colorsused*4;
   file_header->filesize     = file_header->bitmapoffset +
                               bmheader->sizeofbitmap;
   
   fp = fopen(file_name, "wb");
   assert ( (fp != NULL) && "create_allocate_bmp_file() file creation error");
   
   // 14-byte BMP file header.
   insert_ushort_into_buffer(buffer, 0, file_header->filetype);
   fwrite(buffer, 1, 2, fp);

   insert_ulong_into_buffer(buffer, 0, file_header->filesize);
   fwrite(buffer, 1, 4, fp);

   insert_short_into_buffer(buffer, 0, file_header->reserved1);
   fwrite(buffer, 1, 2, fp);

   insert_short_into_buffer(buffer, 0, file_header->reserved2);
   fwrite(buffer, 1, 2, fp);

   insert_ulong_into_buffer(buffer, 0, file_header->bitmapoffset);
   fwrite(buffer, 1, 4, fp);

   // 40-byte bit map header.
   insert_ulong_into_buffer(buffer, 0, bmheader->size);
   fwrite(buffer, 1, 4, fp);

   insert_long_into_buffer(buffer, 0, bmheader->width);
   fwrite(buffer, 1, 4, fp);

   insert_long_into_buffer(buffer, 0, bmheader->height);
   fwrite(buffer, 1, 4, fp);

   insert_ushort_into_buffer(buffer, 0, bmheader->planes);
   fwrite(buffer, 1, 2, fp);

   insert_ushort_into_buffer(buffer, 0, bmheader->bitsperpixel);
   fwrite(buffer, 1, 2, fp);

   insert_ulong_into_buffer(buffer, 0, bmheader->compression);
   fwrite(buffer, 1, 4, fp);

   insert_ulong_into_buffer(buffer, 0, bmheader->sizeofbitmap);
   fwrite(buffer, 1, 4, fp);

   insert_ulong_into_buffer(buffer, 0, bmheader->horzres);
   fwrite(buffer, 1, 4, fp);

   insert_ulong_into_buffer(buffer, 0, bmheader->vertres);
   fwrite(buffer, 1, 4, fp);

   insert_ulong_into_buffer(buffer, 0, bmheader->colorsused);
   fwrite(buffer, 1, 4, fp);

   insert_ulong_into_buffer(buffer, 0, bmheader->colorsimp);
   fwrite(buffer, 1, 4, fp);

   // blank color table. 256 entries, each 4 bytes.
   buffer[0] = 0x00;
   for (i=0; i<(256*4); i++)
     fwrite (buffer, 1, 1, fp);

   // dump a 0 image.
   buffer[0] = 0x00;

   for (i=0; i<bmheader->sizeofbitmap; i++)
     fwrite (buffer, 1, 1, fp);
   
   fclose(fp);
}



void write_bmp_image ( char   *file_name, short  array[HEIGHT][WIDTH] ) {
  char   *buffer, c;
  FILE   *image_file;
  int    pad = 0,  position;
  int    bytes, i, j;
  long   height = 0, width = 0;
  struct bitmapheader  bmheader;
  struct bmpfileheader file_header;
  struct ctstruct rgb[GRAY_LEVELS+1];
  union  short_char_union scu;

  read_bmp_file_header (file_name, &file_header);
  read_bm_header (file_name, &bmheader);
  
  height = bmheader.height;
  width  = bmheader.width;
  if (height < 0) height = height*(-1);
  
  buffer = (char  *) malloc(width * sizeof(char ));
  for (i=0; i<width; i++)
    buffer[i] = '\0';
  
  image_file = fopen (file_name, "rb+");
  // color table 
  position   = fseek(image_file, 54, SEEK_SET);
  for (i=0; i<GRAY_LEVELS+1; i++){
    rgb[i].blue  = i;
    rgb[i].green = i;
    rgb[i].red   = i;
  }
  
  for (i=0; i<bmheader.colorsused; i++){
    buffer[0] = rgb[i].blue;
    fwrite(buffer , 1, 1, image_file);
    buffer[0] = rgb[i].green;
    fwrite(buffer , 1, 1, image_file);
    buffer[0] = rgb[i].red;
    fwrite(buffer , 1, 1, image_file);
    buffer[0] = 0x00;
    fwrite(buffer , 1, 1, image_file);
  }
  
  position = fseek (image_file, file_header.bitmapoffset, SEEK_SET);
  pad = calculate_pad(width);
  
  for (i=0; i<height; i++) {
    for (j=0; j<width; j++) {
      if (bmheader.bitsperpixel == 8) {
	scu.s_num = 0;
	if(bmheader.height > 0)
	  scu.s_num = array[height-1-i][j];
	else
	  scu.s_num = array[i][j];
	buffer[j] = scu.s_alpha[0];
      } 
    } 
    
    bytes = fwrite(buffer, 1, width, image_file);
    
    if (pad != 0) {
      for(j=0; j<pad; j++)
	buffer[j] = 0x00;
      fwrite(buffer, 1, pad, image_file);
    }  
  } 
  
  fclose (image_file);
  free (buffer);
} 


// padding needed at the end of each row of pixels.
int calculate_pad (long width) {
   int pad = 0;
   pad = ( (width%4) == 0) ? 0 : (4-(width%4));
   return(pad);
}  


// creates an image file with parameters copied from a given input image file.
void create_image_file ( char *in_name, char *out_name ) {
  struct bmpfileheader      bmp_file_header;
  struct bitmapheader       bmheader;
  
  read_bmp_file_header(in_name,  &bmp_file_header);
  read_bm_header(in_name, &bmheader);
  create_allocate_bmp_file (out_name,  &bmp_file_header,  &bmheader);

} 
