// Use inside ./pixel_to_image.sh script.

#include <common.h>
#include <image_size.h>
#include <imageio.h>
#include <out_image.h>

int main (int argc, char *argv[]) {
  char  in_image_name[2048],  out_image_name[2048];
  int   i, j;
  short out_image[HEIGHT][WIDTH];
   
  strcpy (in_image_name,  argv[1]);
  strcpy (out_image_name, argv[2]);
  create_image_file (in_image_name, out_image_name);

  const long height = HEIGHT; const long width = WIDTH; 
  const long bits_per_pixel = 8;
  const int high = 0; const int threshold = 1; // don't change these values.

  for (i=0; i<height; i++) { // 1D to 2D
    for (j=0; j<width; j++)
      out_image[i][j] =  out_image_1D [(height * i) + j]; 
  }

  write_bmp_image (out_image_name, out_image);

  printf ("\n");
  return 0;

} 


