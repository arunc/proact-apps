#pragma once
#ifndef IMAGEIO_H
#define IMAGEIO_H


#include "common.h"
#include "image_size.h"
#include <assert.h>

// in image_to_pixel.c, image_size.h is not available.
// does not affect anything, since in image_to_pixel.c, its only reading.
#ifndef HEIGHT
#error does not work
#define HEIGHT 512
#endif

#ifndef WIDTH
#define WIDTH 512
#endif



int is_little_endian ();

void extract_long_from_buffer (char buffer[], int lsb, int start, long *number);
void extract_ulong_from_buffer(char buffer[], int lsb, int start, unsigned long * number);
void extract_short_from_buffer (char buffer[], int lsb, int start, short *number);
void extract_ushort_from_buffer (char  buffer[], int lsb, int start, 
				 unsigned short *number );

void insert_short_into_buffer ( char  buffer[], int   start, short number);
void insert_ushort_into_buffer ( char  buffer[], int   start, unsigned short number);
void insert_long_into_buffer ( char buffer[], int  start, long number);
void insert_ulong_into_buffer ( char buffer[], int  start, unsigned long number);

void read_bmp_file_header ( char *file_name, struct bmpfileheader *file_header);
void read_bm_header ( char *file_name, struct bitmapheader *bmheader);

void read_color_table ( char   *file_name, struct ctstruct *rgb, int    size);
void flip_image_array ( short  **the_image, long   cols, long rows );
void read_bmp_image ( char  *file_name, short **array);
void create_allocate_bmp_file ( char  *file_name, struct bmpfileheader *file_header,
				struct bitmapheader  *bmheader);
void write_bmp_image ( char   *file_name, short  array[HEIGHT][WIDTH] );

int calculate_pad (long width);

void create_image_file ( char *in_name, char *out_name);

#endif
