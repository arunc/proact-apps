Sample Edge Detection Application

IMPORTANT:
FIRST you must clone git@gitlab.com:arunc/proact-processor.git  
SECOND, build the RISC-V tools as in proact-processor/build/HowToBuild.txt, and 
THIRD, set the RISC-V environment variable as in proact-processor/set_env.sh
This project (proact-processor) is independent of proact-apps

Follow the steps to create a proact application for edge-detection, run it in zedboard, and 
compare the speed improvements when approximations are turned on.

Concise direct way to do this.

# BOOT UP THE ZEDBOARD and CONNECT via ETHERNET
cd .. ; ./utils/prepare_zedboard.sh; cd -   # DO ONLY ONCE PER SESSION.

# For approximation disabled. 
# Results will be in ./pics/Znyq256.bmp_appx-1.bmp ./pics/Znyq256.bmp_appx-1.rpt
./scripts/process_image.sh ./pics/Znyq256.bmp 256 256 -1 

# Approximation enabled. 
# Results will be in ./pics/Znyq256.bmp_appx20.bmp ./pics/Znyq256.bmp_appx20.rpt
./scripts/process_image.sh ./pics/Znyq256.bmp 256 256 20 

Now, in case of errors, either debug the steps in the script/process_image.sh OR
follow the steps below to identify where the problem is.
Even the detailed steps are with scripts, you may want to read and understand them to get a proper 
understanding.

----------------------------------------------------------------------------------------------------
Detailed Steps for approximations disabled:

1) ./scripts/image_to_pixel.sh ./pics/lena512.bmp in_image.h 512 512  # generates in_image.h and image_size.h
2) sed -i 's/APPROX_LEVEL.*/APPROX_LEVEL -1/g' edge_detect/approximation_level.h 
  # Or alternately hand modify the APPROX_LEVEL in the edge_detect/approximation_level.h to -1
  # -1 means disable approximations completely.
3) make clean; make riscv    # generates the edge_detect.riscv binary to be run on zedboard.

4) # BOOT UP THE ZEDBOARD and CONNECT via ETHERNET
5) cd .. ; ./utils/prepare_zedboard.sh; cd -   # DO ONLY ONCE PER SESSION.

6) ./scripts/copy_to_zedboard.sh edge_detect.riscv

7) ssh root@192.168.1.5   # LOGIN TO ZEDBOARD 
8) ./fesvr-zedboard edge_detect.riscv  > approx_disabled.txt   # ** IN THE ZEDBOARD **
9) exit   # LOGOUT FROM ZEDBOARD and COME BACK TO HOST.

10) ./scripts/copy_from_zedboard.sh approx_disabled.txt   # copy back the results to host.
11) ./scripts/grep_report.sh approx_disabled.txt
    # This will convert the pixel info to proper BMP format.
12)./scripts/pixel_to_image.sh approx_disabled.txt.out approx_disabled.bmp
13) # NOW check the BMP image approx_disabled.bmp in an image viewer.

----------------------------------------------------------------------------------------------------
Detailed Steps for approximations enabled :: everything same as above except (2) 

1) ./image_to_pixel.sh ./pics/lena512.bmp in_image.h 512 512  # generates in_image.h and image_size.h
2) sed -i 's/APPROX_LEVEL.*/APPROX_LEVEL 40/g' edge_detect/approximation_level.h 
  # Or alternately hand modify the APPROX_LEVEL in the edge_detect/approximation_level.h to 40
  # 40 means enable approximations and set to level 40.
3) make clean; make riscv    # generates the edge_detect.riscv binary to be run on zedboard.

4) # BOOT UP THE ZEDBOARD and CONNECT via ETHERNET
5) cd .. ; ./utils/prepare_zedboard.sh; cd -  # NO NEED TO DO THIS REPEATEDLY 

6) ./scripts/copy_to_zedboard.sh edge_detect.riscv

7) ssh root@192.168.1.5   # LOGIN TO ZEDBOARD 
8) ./fesvr-zedboard edge_detect.riscv  > approx_enabled.txt   # ** IN THE ZEDBOARD **
9) exit   # LOGOUT FROM ZEDBOARD and COME BACK TO HOST.

10) ./scripts/copy_from_zedboard.sh approx_enabled.txt   # copy back the results to host.
11) ./scripts/grep_report.sh approx_enabled.txt
    # This will convert the pixel info to proper BMP format.
12)./scripts/pixel_to_image.sh approx_enabled.txt.out approx_enabled.bmp
13) # NOW check the BMP image approx_enabled.bmp in an image viewer.


**** COMPARE THE RESULTS approx_disabled.txt.rpt and approx_enabled.txt.rpt
**** AND CHECK THE MCYCLE entry (the number of machine cycles taken for computations)
**** Should see a considerable difference there.

----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
Flow details.

There is some problem with io read/write with the zedboard.
ToDo: debug later.
1) For now, the image is converted to a pixel file. (image_to_pixel.exe X86 application)
2) With this pixel file, the edge-detect application is hardcoded into 
   a single RISCV executable. (Makeflow make zedboard, then scp the executable to the zedboard)
3) RISCV executable runs in the zedboard and dumps the result to stdout 
   in the pixel form. (in the zedboard, so scp the results back to the system)
4) pixels are further packed to image data by another X86 application.  
   (pixel_to_image.exe X86 application)





// IGNORE - completely removed from the flow.
Use very small images (64 x 64) with emulator and emulator-x.
Else these will take for ever to complete.
 
