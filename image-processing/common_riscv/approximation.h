#pragma once
#ifndef APPROXIMATION_H
#define APPROXIMATION_H


// Approximation routines.
// sets and queries the approximation status of Proact.
// Note : these are hardware level approximations employed in floating point unit.
// Since immediate is used to simplify hardware, only some values are given here.
// ToDo: expand this library to for more ranges.



static int __approx_level__ = 0;
static int __exp_level__ = 0;
static int __approx_enabled__ = 0;


// Immediate value to be set for SXL instruction is formed like this :
// 2bits exp-approx + 1bit dont-care + 8bits immediate + 1 bit enable.
// so for setting an approximation level of 5 and enabling it 
// the immediate value = b'000101 :: b'1 = b'1011 = d'11 (in decimal)
//
//
// 2bits :: 0 :: 8bits  :: 1bit
// exp-approximation (bb) : don't care (b) : nMaskBits (bbbb bbbb) : enable-approx (b) 

// IMPORTANT : Exp Approximation Scheme
// 00 : no exp approximation 
// 01 : if the exp difference is more than 100, it will be approximated. (ax10^105 / b = ax10^104,  a/ bx10^111)
// 10 : if the exp difference is more than 50, it will be approximated. (ax10^60 / b,  a/ bx10^59)
// 11 : if the exp difference is more than 50, it will be approximated. (ax10^11 / b,  a/ bx10^20 = a x 10^-19)

//-----------------------------------------------

// disables approximation
#define __SXL_M1__ asm volatile ("sxl x0,  00(x0);" )

// below one is approximation-enable but without any maskbits.
// i.e. full look-up table.
#define __SXL_00__ asm volatile ("sxl x0,  01(x0);" )

// todo: put the actual limit below.
// 2039 is the somwhere near the maximum +ve number possible. (1 bit sign + 11 bits)
// beyond that get a 2's compliment number and put -ve.
#define __SXL_10__EXP0 asm volatile ("sxl x0,  21(x0);" )    //  00 0 0000 1010 1
#define __SXL_10__EXP1 asm volatile ("sxl x0,  1045(x0);" )  //  01 0 0000 1010 1
#define __SXL_10__EXP2 asm volatile ("sxl x0,  -2027(x0);" ) //  10 0 0000 1010 1 => 01 1 1111 0101 0 + 1 => 01 1 1111 0101 1
#define __SXL_10__EXP3 asm volatile ("sxl x0,  -1003(x0);" ) //  11 0 0000 1010 1 => 00 1 1111 0101 0 + 1 => 00 1 1111 0101 1

#define __SXL_20__EXP0 asm volatile ("sxl x0,  29(x0);" )    //  00 0 0001 0100 1
#define __SXL_20__EXP1 asm volatile ("sxl x0,  1065(x0);" )  //  01 0 0001 0100 1
#define __SXL_20__EXP2 asm volatile ("sxl x0,  -2007(x0);" ) //  10 0 0001 0100 1 => 01 1 1110 1011 0 + 1 => 01 1 1110 1011 1
#define __SXL_20__EXP3 asm volatile ("sxl x0,  -983(x0);" )  //  11 0 0001 0100 1 => 00 1 1110 1011 0 + 1 => 00 1 1110 1011 1

#define __SXL_30__EXP0 asm volatile ("sxl x0,  61(x0);" )    //  00 0 0001 1110 1
#define __SXL_30__EXP1 asm volatile ("sxl x0,  542(x0);" )   //  01 0 0001 1110 1
#define __SXL_30__EXP2 asm volatile ("sxl x0,  1054(x0);" )  //  10 0 0001 1110 1
#define __SXL_30__EXP3 asm volatile ("sxl x0,  1566(x0);" )  //  11 0 0001 1110 1

#define __SXL_40__EXP0 asm volatile ("sxl x0,  81(x0);" )    //  00 0 0010 1000 1
#define __SXL_40__EXP1 asm volatile ("sxl x0,  1105(x0);" )  //  01 0 0010 1000 1
#define __SXL_40__EXP2 asm volatile ("sxl x0,  -1967(x0);" ) //  10 0 0010 1000 1 => 01 1 1101 0111 0 + 1 => 01 1 1101 0111 1
#define __SXL_40__EXP3 asm volatile ("sxl x0,  -943(x0);" )  //  11 0 0010 1000 1 => 00 1 1101 0111 0 + 1 => 00 1 1101 0111 1


// 1. enables approximations and
// 2. set the approximation level to the given level.
// 3. set the exp-approximation to the given level.
// note: any value less than 0, disables approximation.
//static void set_approximation_level (int level) {

static void set_approximation_level (int level, int exp) {
#ifdef _ASSERT_H
  assert (level > 64  && level < 0 && "Approximation level outside range (0 < level < 64)"); 
#endif
  __approx_enabled__ = 0;
  if (level < 0)  { __SXL_M1__ ; __approx_level__ = 0; return;} //disable . no approx
  __approx_enabled__ = 1;

  if (level == 0) { __SXL_00__ ; __approx_level__ = 0; return;} //only enable. no approx

  if (level <= 10 && exp == 0) { __SXL_10__EXP0 ; __approx_level__ = 10; __exp_level__ = 0; return;} // en + level 10
  if (level <= 10 && exp == 1) { __SXL_10__EXP1 ; __approx_level__ = 10; __exp_level__ = 1; return;} // en + level 10
  if (level <= 10 && exp == 2) { __SXL_10__EXP2 ; __approx_level__ = 10; __exp_level__ = 2; return;} // en + level 10
  if (level <= 10 && exp >= 3) { __SXL_10__EXP3 ; __approx_level__ = 10; __exp_level__ = 3; return;} // en + level 10

  if (level <= 20 && exp == 0) { __SXL_20__EXP0 ; __approx_level__ = 20; __exp_level__ = 0; return;} // en + level 20
  if (level <= 20 && exp == 1) { __SXL_20__EXP1 ; __approx_level__ = 20; __exp_level__ = 1; return;} // en + level 20
  if (level <= 20 && exp == 2) { __SXL_20__EXP2 ; __approx_level__ = 20; __exp_level__ = 2; return;} // en + level 20
  if (level <= 20 && exp >= 3) { __SXL_20__EXP3 ; __approx_level__ = 20; __exp_level__ = 3; return;} // en + level 20

  if (level <= 30 && exp == 0) { __SXL_30__EXP0 ; __approx_level__ = 30; __exp_level__ = 0; return;} // en + level 30
  if (level <= 30 && exp == 1) { __SXL_30__EXP1 ; __approx_level__ = 30; __exp_level__ = 1; return;} // en + level 30
  if (level <= 30 && exp == 2) { __SXL_30__EXP2 ; __approx_level__ = 30; __exp_level__ = 2; return;} // en + level 30
  if (level <= 30 && exp >= 3) { __SXL_30__EXP3 ; __approx_level__ = 30; __exp_level__ = 3; return;} // en + level 30

  if (level >= 31 && exp == 0) { __SXL_40__EXP0 ; __approx_level__ = 40; __exp_level__ = 0; return;} // en + level 40
  if (level >= 31 && exp == 1) { __SXL_40__EXP1 ; __approx_level__ = 40; __exp_level__ = 1; return;} // en + level 40
  if (level >= 31 && exp == 2) { __SXL_40__EXP2 ; __approx_level__ = 40; __exp_level__ = 2; return;} // en + level 40
  if (level >= 31 && exp >= 3) { __SXL_40__EXP3 ; __approx_level__ = 40; __exp_level__ = 3; return;} // en + level 40



}






// returns the current approximation level.
static int get_approximation_level () {
  return __approx_level__;
}


// enables the approximations. The approximation level will continue as was before.
static void enable_approximations () {
  __approx_enabled__ = 1;
  set_approximation_level (__approx_level__, 0);
}

// disables the approximations. Does not do change the approximation level earlier set.
static void disable_approximations () {
  __approx_enabled__ = 0;
  __SXL_M1__ ;
}

// Query the status of approximations.
static int is_approximation_enabled () {
  return __approx_enabled__;
}


#endif
