#!/usr/bin/env bash
# 
# Linux X86 tool chain (Not RISCV)
# converts pixels in text format to a BMP image.
# used inside detect_edge.sh
#--------------------------------------------------------------------------

if [ -z $1 ]; then echo "Usage: ./pixel_to_image.sh <pixel.txt> <image.bmp> <template.bmp>"; exit; fi
if [ -z $2 ]; then echo "Usage: ./pixel_to_image.sh <pixel.txt> <image.bmp> <template.bmp>"; exit; fi
if [ ! -e $1 ]; then echo "[e] $1 does not exist!"; exit; fi
if [ -e $2 ]; then echo "[w] $2 moved to ${2}.old"; mv ${2} ${2}.old; fi


#--------------------------------------------------------------------------
# Get the image size from image_size.h
HEIGHT=`grep HEIGHT image_size.h | awk '{print $3}' `
WIDTH=`grep WIDTH image_size.h | awk '{print $3}' `

# Supports only HEIGHT x WIDTH image.
# Number of rows in pixel file = HEIGHT x WIDTH (written in 1D format)
LINES=`wc -l ${1} | awk '{print $1}' `
ROWS=$((HEIGHT * WIDTH))
SEDRANGE=$((ROWS - 1))

# Do a sanity check of these ranges.
if [ "$LINES" -ne  "$ROWS" ] ; then
    echo "ERROR";
    echo "[e] Number of lines in $1 is not equal to the number of pixels";
    echo "[i] line count = $LINES and pixel count = $ROWS   (HEIGHT x  WIDTH)";
    echo "[i] HEIGHT = $HEIGHT , WIDTH = $WIDTH   (values taken from image_size.h)"
    
    # Block further scripts, if there is an error.
    if [ -e out_image.h ]; then mv out_image.h pixel_old.h; fi
    exit;
fi

#--------------------------------------------------------------------------

# 1. Generate the header file for pixels
if [ -e out_image.h ]; then mv out_image.h pixel_old.h; fi
touch out_image.h
printf "short out_image_1D [${ROWS}] = { \n" >> out_image.h
sed -e "1,${SEDRANGE}s/\$/,/g" $1 >> out_image.h
printf "\n};\n\n" >> out_image.h


# 2. Compile and dump the image.
#template_image=./pics/lena${HEIGHT}.bmp
template_image=$3

echo "pixel_to_image.exe ${template_image} $2  ";
\rm -f pixel_to_image.exe # Must recompile for changed height x width. 
if [ ! -e pixel_to_image.exe  ]; then
    g++  ./common_x86/imageio.c  ./common_x86/pixel_to_image.c -I. -I./common_x86 -o pixel_to_image.exe
fi

./pixel_to_image.exe ${template_image} ${2}

