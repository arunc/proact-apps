#!/usr/bin/env bash
#
# Usage : process_image.sh <image> <width> <height> <approximation-level>
#
#  e.g. : process_image.sh pics/lena256.bmp 256 256  -1  # approximations completely disabled.
#       : process_image.sh pics/lena256.bmp 256 256  20  # approximation enabled and set at level 20
#       : process_image.sh pics/zynq.bmp 256 256 20   0  # approximation enabled and set at level 20
#
# Result will be output to the host terminal.
#-------------------------------------------------------------------------------

print_usage () {
    echo "Usage: process_image.sh <image> <width> <height> <approximation-level>";
    echo "e.g. : process_image.sh pics/lena256.bmp 256 256  -1  # approximations completely disabled."
    echo "       process_image.sh pics/lena256.bmp 256 256  20  # approximation enabled and set at level 20"
    echo "       process_image.sh pics/zynq.bmp 256 256 20   0  # approximation enabled and set at level 20"
    echo "       "
}

if [ -z $1 ]; then echo "Error: image not provided!"; print_usage; exit; fi
if [ -z $2 ]; then echo "Error: width not provided!"; print_usage; exit; fi
if [ -z $2 ]; then echo "Error: height not provided!"; print_usage; exit; fi
if [ -z $3 ]; then echo "Error: approximation-level not provided!"; print_usage; exit; fi


img=$1
width=$2
height=$3
appx=$4
#-------------------------------------------------------------------------------
command -v sshpass >/dev/null 2>&1 || { echo >&2 "Program sshpass is not installed. Either install sshpass or copy all these files manually.  Aborting."; exit 1; }
command -v scp >/dev/null 2>&1 || { echo >&2 "Program scp is not installed. Must have scp and ssh to communicate with zedboard.  Aborting."; exit 1; }

copy_to_zedboard () {
    sshpass -p 'root' scp $1 root@192.168.1.5:~/
}

copy_from_zedboard () {
    sshpass -p 'root' scp root@192.168.1.5:~/${1} .    
}

run_in_zedboard () {
    sshpass -p 'root' ssh root@192.168.1.5 -t ./exec_zedboard.sh $1 $2
}
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
#echo "./scripts/image_to_pixel.sh $img in_image.h $width $height"
./scripts/image_to_pixel.sh $img in_image.h $width $height
#exit
sed -i "s/APPROX_LEVEL.*/APPROX_LEVEL ${appx}/g" edge_detect/approximation_level.h 
make clean && make riscv
copy_to_zedboard       edge_detect.riscv
copy_to_zedboard       ./scripts/exec_zedboard.sh
run_in_zedboard        edge_detect.riscv image_appx${appx}.txt
copy_from_zedboard     image_appx${appx}.txt
./scripts/grep_report.sh       image_appx${appx}.txt
./scripts/pixel_to_image.sh    image_appx${appx}.txt.out ${img}_appx${appx}.bmp $img
cp image_appx${appx}.txt.rpt   ${img}_appx${appx}.rpt

echo  
echo "RESULTS :: ${img}_appx${appx}.rpt (REPORT)    ${img}_appx${appx}.bmp (IMAGE)"
echo  
cat ${img}_appx${appx}.rpt


#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
