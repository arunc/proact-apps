#!/usr/bin/env bash
#
# Linux x86 tool chain (Not RISCV)
# converts BMP image to pixel-text
# 
#--------------------------------------------------------------------------


if [ -z $1 ]; then echo "Usage: ./image_to_pixel.sh <image.bmp> <pixel.c> <height> <width>"; exit; fi
if [ -z $2 ]; then echo "Usage: ./image_to_pixel.sh <image.bmp> <pixel.c> <height> <width>"; exit; fi
if [ -z $4 ]; then echo "Error: height and width must be provided";
    echo "Usage: ./image_to_pixel.sh <image.bmp> <pixel.c> <height> <width>"; exit; 
fi
if [ ! -e $1 ]; then echo "[e] $1 does not exist!"; exit; fi
if [ -e $2 ];   then echo "[w] $2 moved to ${2}.old"; mv $2 ${2}.old; fi

# Dumps HEIGHTxWIDTH to image_size.h

\rm -f image_to_pixel.exe
if [ ! -e  image_to_pixel.exe ]; then
    g++ ./common_x86/image_to_pixel.c ./common_x86/imageio.c -I. -I./common_x86 -o image_to_pixel.exe
fi

./image_to_pixel.exe $1 $2 $3 $4 



