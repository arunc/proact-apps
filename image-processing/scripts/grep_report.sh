#!/usr/bin/env bash

if [ -z $1 ]; then echo "Usage: grep_report.sh results.txt [outputs : results.txt.rpt and results.txt.out]"; exit; fi

RRR=$RANDOM
if [ -e ${1}.rpt.old ]; then mv ${1}.rpt.old ${1}.rpt.old.${RRR}; fi
if [ -e ${1}.rpt ]; then mv ${1}.rpt ${1}.rpt.old; fi

if [ -e ${1}.out.old ]; then mv ${1}.out.old ${1}.out.old.${RRR}; fi
if [ -e ${1}.out ]; then mv ${1}.out ${1}.out.old; fi

touch ${1}.rpt
grep 'info'     $1  >> ${1}.rpt
grep 'mcycle'   $1  >> ${1}.rpt
grep 'minstret' $1  >> ${1}.rpt
#grep '='        $1  >> ${1}.rpt


grep -E -v 'info|mcycle|minstret|=' $1 > ${1}.out
